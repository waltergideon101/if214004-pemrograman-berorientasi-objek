# Job Interview 1

## Outcome
- **Mampu menjelaskan dan mengimplementasikan dasar OOP**
- Mempu menganalisis permasalahan dunia nyata dan menerjemahkannya dalam solusi berbasis OOP

## Deskripsi
1. Pilih produk aplikasi / game digital yang Kamu suka (IG / Tiktok / BCA Mobile / Gojek / Grab / Mobile Legend / Photoshop / Office / dsb.)
2. Buat list use case prioritas dari produk digital tersebut
3. Desain dan implementasikan dalam bentuk program sederhana berbasis Object Oriented Programming dengan ketentuan
    - Buat class diagram sederhananya  
    - Mengimplementasikan 4 pilar OOP dengan **tepat**
        1. Encapsulation
        2. Abstraction
            - The why
            - Abstract class
            - Interface
        3. Inheritance
        4. Polymorphism
            - Overriding
            - Overloading
    - Mengimplementasikan keseluruhan fitur OOP
        - Constructor
        - Final
        - Static
        - Association, Aggregation, Composition
        - Modifier
4. Beri interface minimal dalam bentuk Command Line interface
5. Buat video presentasi dari produk yang dibuat (audience publik)
6. Wawancara: 
    - Mampu menjelaskan OOP secara komprehensif menggunakan project yang dibuat

## Registrasi Repo Job Interview
[Registrasi di sini: nama, nim, kelas, alamat repo interview](https://gitlab.com/marchgis/march-ed/2023/courses/if214005-praktikum-pemrograman-berorientasi-objek/-/issues/1)

## Contoh Pengerjaan

### Pilih Aplikasi
> Instagram

### Use Case
User | Bisa | Nilai prioritas
--- | --- | --- 
user | posting post | 100
user | komentar post user lain | 90
user | kirim message | 80
user | like post | 70
public | registrasi | 80
user | login | 70
manajemen ig | melihat statistik penggunaan | 90
manajemen ig | menindak lanjut konten buruk | 70 
dsb. | | 

### Class Diagram
- [Contoh](https://glints.com/id/lowongan/class-diagram-adalah/)

### Coding
```java

/*
Instagram
1. Bisa posting Konten
2. Bisa respon Konten: like, comment, bookmark, share
3. Bisa kirim pesan personal
4. Bisa follow

InstagramApp
- contents: Content[]
    - id
    - media: Media[]
    - responses: Response[]
- users: User[]
    - id
    - username
    - displayName
    - photoURL
    - followings: User[]
    - followers: User[]
    - contents: Content[]
    - post(Content content)
    - respond(Response response)
*/

import java.util.ArrayList;

class User {
    private String username;
    public User(String username) {
        this.username = username;
    }
    
    public String getUsername() {
        return this.username;
    }
}

class InstagramApp {
    private String version = "0.0.1";
    public void showVersion() {
        System.out.println("Version " + this.version);
    }
    
    public void createUser(User newUser) {
        this.users.add(newUser);
        System.out.println("User " + newUser.getUsername() + " berhasil ditambahkan, jumlah user Instagram saat ini ada " + this.users.size());
    }
    
    private ArrayList<User> users = new ArrayList<User>(); 
}

public class Main
{
    public static void main(String[] args) {
        InstagramApp app = new InstagramApp();
        app.showVersion();
        
        User user1 = new User("syamil_mr");
        app.createUser(user1);
        
        User user2 = new User("the_dian");
        app.createUser(user2);
    }
}
```
